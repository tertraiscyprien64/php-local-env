# Environement Local PHP
##### par Cyprien TERTRAIS

Cet environement intégre plusieurs fonctionalitées:

  - Phpmyadmin
  - Php v7
  - Mysql

# Installation

- Installer Docker
- Commande pour lancer: ```$  docker-compose up -d ```
- Commande pour arreter tous les containers: ```$  docker container stop $(docker container ls -aq) ```
# Contact

cyprien.tertrais@hotmail.com